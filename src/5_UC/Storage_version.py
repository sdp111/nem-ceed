def run_uc_model(fname, fix_hydro=True):
    # Model
    # -----
    m = ConcreteModel()

    # Sets
    # ----
    # Generators
    if fix_hydro:
        mask = (df_g['SCHEDULE_TYPE'] == 'SCHEDULED') & df_g['FUEL_CAT'].isin(['Fossil']) & ~pd.isnull(
            df_g['MIN_ON_TIME'])
        m.Generators = Set(initialize=df_g[mask].index)

        mask = (df_g['SCHEDULE_TYPE'] == 'SCHEDULED') & df_g['FUEL_CAT'].isin(['Hydro']) & ~pd.isnull(
            df_g['MIN_ON_TIME'])
        m.HYDRO = Set(initialize=df_g[mask].index)

    else:
        mask = (df_g['SCHEDULE_TYPE'] == 'SCHEDULED') & df_g['FUEL_CAT'].isin(['Hydro', 'Fossil']) & ~pd.isnull(
            df_g['MIN_ON_TIME'])
        m.Generators = Set(initialize=df_g[mask].index)
        m.HYDRO = Set()

    # NEM zones
    m.NEM_Zones = Set(initialize=df_rm.index)

    # NEM regions
    m.NEM_Regions = Set(initialize=df_rm['NEM_REGION'].unique())

    # HVDC links
    m.HVDC_Lines = Set(initialize=df_hvdc_C.index)

    # AC lines
    m.AC_Lines = Set(initialize=df_ac_C.index)

    # (Dictionary to convert between timestamps and time indices)
    t_dict = {NEM_zone + 1: k for NEM_zone, k in enumerate(df_zd[:48].index)}

    # Time indices
    m.Time = Set(initialize=list(t_dict.keys()), ordered=True)

    # Parameters
    # ----------
    # Constant linear variable cost (SRMC)

    def SRMC_rule(m, generator):
        return float(df_g.loc[generator, 'SRMC_2016-17'])

    m.SRMC = Param(m.Generators, initialize=SRMC_rule)

    # No load cost
    def C_NL_rule(m, generator):
        if df_g.loc[generator, 'FUEL_CAT'] == 'Hydro':
            return float(0)
        else:
            return float((df_g.loc[generator, 'NL_FUEL_CONS'] * df_g.loc[generator, 'HEAT_RATE'] * df_g.loc[
                generator, 'FC_2016-17']))

    m.C_NL = Param(m.Generators, initialize=C_NL_rule)

    # Maximum power output
    def P_MAX_rule(m, generator):
        return float(df_g.loc[generator, 'REG_CAP'])

    m.P_MAX = Param(m.Generators, initialize=P_MAX_rule)

    # Minimum power output
    def P_MIN_rule(m, generator):
        min_gen = df_g.loc[generator, 'MIN_GEN']

        # If no data for min gen as % of nameplate capacity, return 0
        if pd.isnull(min_gen):
            return float(0)
        else:
            return float(min_gen)

    m.P_MIN = Param(m.Generators, initialize=P_MIN_rule)

    # Hydro output
    def HYDRO_OUTPUT_rule(m, hydro, t):
        return float(df_scada.loc[t_dict[t], hydro])

    m.HYDRO_OUTPUT = Param(m.HYDRO, m.Time, initialize=HYDRO_OUTPUT_rule)

    # Time interval length in hours
    m.DELTA = Param(initialize=0.5)

    # Ramp up capability
    def RU_rule(m, generator):
        return float(df_g.loc[generator, 'RR_UP'] * m.DELTA.value)

    m.Ramp_UP = Param(m.Generators, initialize=RU_rule)

    # Ramp down capability
    def RD_rule(m, generator):
        return float(df_g.loc[generator, 'RR_DOWN'] * m.DELTA.value)

    m.Ramp_Down = Param(m.Generators, initialize=RD_rule)

    # Minimum off time expressed in terms of time intervals
    def Off_Time_rule(m, generator):
        return float(df_g.loc[generator, 'MIN_OFF_TIME'] / m.DELTA.value)

    m.Min_Off_Time = Param(m.Generators, initialize=Off_Time_rule)

    # Minimum on time expressed in terms of time intervals
    def On_Time_rule(m, generator):
        return float(df_g.loc[generator, 'MIN_ON_TIME'] / m.DELTA.value)

    m.TU = Param(m.Generators, initialize=On_Time_rule)

    # Demand for each NEM zone
    def Demand_Zone_rule(m, NEM_zone, t):
        return float(df_zd.loc[t_dict[t], NEM_zone])

    m.Zone_Demand = Param(m.NEM_Zones, m.Time, initialize=Demand_Zone_rule)

    # HVDC incidence matrix
    def HVDC_C_rule(m, dc_line, NEM_zone):
        return float(df_hvdc_C.loc[dc_line, NEM_zone])

    m.HVDC_C = Param(m.HVDC_Lines, m.NEM_Zones, initialize=HVDC_C_rule)

    # AC incidence matrix
    def AC_C_rule(m, ac_line, NEM_zone):
        return float(df_ac_C.loc[ac_line, NEM_zone])

    m.AC_C = Param(m.AC_Lines, m.NEM_Zones, initialize=AC_C_rule)

    # Intermittent generation for each zone
    def P_W_rule(m, NEM_zone, t):
        wind_output = float(df_inter.loc[t_dict[t], NEM_zone])
        if wind_output < 0:
            return float(0)
        else:
            return wind_output

    m.Zone_Wind_Power_Parameter = Param(m.NEM_Zones, m.Time, initialize=P_W_rule)

    # Start-up costs
    def C_SU_rule(m, generator):
        return float(df_g.loc[generator, 'SU_COST_COLD'] * m.P_MIN[generator])

    m.C_SU = Param(m.Generators, initialize=C_SU_rule)

    # Start-up ramp rate
    def SU_D_rule(m, generator):
        ru_intervals = m.P_MIN[generator] / (df_g.loc[generator, 'RR_STARTUP'] * m.DELTA.value)
        if ru_intervals > 1:
            return float(ceil(ru_intervals))
        else:
            return float(0)

    m.Startup_Ramp_Rate = Param(m.Generators, initialize=SU_D_rule)

    # Shutdown ramp rate
    def SD_D_rule(m, generator):
        rd_intervals = m.P_MIN[generator] / (df_g.loc[generator, 'RR_SHUTDOWN'] * m.DELTA.value)
        if rd_intervals > 1:
            return float(ceil(rd_intervals))
        else:
            return float(0)

    m.Shutdown_Ramp_Rate = Param(m.Generators, initialize=SD_D_rule)

    # Startup capability
    def SU_rule(m, generator):
        if m.Startup_Ramp_Rate[generator]:
            return m.P_MIN[generator]
        else:
            return float(df_g.loc[generator, 'RR_STARTUP'])

    m.Gen_Startup_Capability = Param(m.Generators, initialize=SU_rule)

    # Shutdown capability
    def SD_rule(m, generator):
        if m.Shutdown_Ramp_Rate[generator]:
            return m.P_MIN[generator]
        else:
            return float(df_g.loc[generator, 'RR_SHUTDOWN'])

    m.Gen_Shutdown_Capability = Param(m.Generators, initialize=SD_rule)

    # Minimum reserve level (up) for each NEM zone
    def D_UP_rule(m, region):
        return float(df_mrl.loc[region])

    m.Min_Zone_Reserve_Up = Param(m.NEM_Regions, initialize=D_UP_rule)

    # Minimum reserve level (down) for each NEM region
    def D_DOWN_rule(m, region):
        return float(df_mrl.loc[region] / 10)

    m.Min_Zone_Reserve_Down = Param(m.NEM_Regions, initialize=D_DOWN_rule)

    # Variables
    # ---------
    # One state for generator
    m.Bin_Generator_On = Var(m.Generators, m.Time, within=Binary)

    # Startup indicator
    m.Bin_Startup_Indicator = Var(m.Generators, m.Time, within=Binary)

    # Shutdown indicator
    m.Bin_Shutdown_Indicator = Var(m.Generators, m.Time, within=Binary)

    # Reserve (up) for each generator
    m.Generator_Reserve_Up = Var(m.Generators, m.Time, within=NonNegativeReals)

    # Resever (down) for each generator
    m.Generator_Reserve_Down = Var(m.Generators, m.Time, within=NonNegativeReals)

    # Wind power output at each node
    m.Zone_Wind_Power_Variable = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)

    # Dispatch for each generator above P_MIN
    m.Generator_Power_Output_Above_Pmin = Var(m.Generators, m.Time, within=NonNegativeReals)

    # Power flow over AC transmission lines
    m.AC_Transmission_Power_Flow = Var(m.AC_Lines, m.Time, within=Reals)

    # Dummy variables used to compute absolute flows over AC link
    m.Power_AC_Interconnector_up = Var(m.AC_Lines, m.Time, within=NonNegativeReals)
    m.Power_AC_Interconnector_lo = Var(m.AC_Lines, m.Time, within=NonNegativeReals)

    # Power flow over HVDC links
    def p_hvdc_rule(m, dc_line, t):
        return (-float(df_hvdc.loc[dc_line, 'REVERSE_LIMIT_MW']), float(df_hvdc.loc[dc_line, 'FORWARD_LIMIT_MW']))

    m.p_hvdc = Var(m.HVDC_Lines, m.Time, bounds=p_hvdc_rule, initialize=0)

    # Dummy variables used to compute absolute flows over HVDC links
    m.p_hvdc_up = Var(m.HVDC_Lines, m.Time, within=NonNegativeReals)
    m.p_hvdc_lo = Var(m.HVDC_Lines, m.Time, within=NonNegativeReals)

    # Expressions
    # -----------
    # Startup cost function
    def Startup_Cost_rule(m, generator):
        return m.C_SU[generator]

    m.Startup_Cost = Expression(m.Generators, rule=Startup_Cost_rule)

    # Total power output for each generator
    def Generator_Power_Output_rule(m, generator, t):
        if m.Startup_Ramp_Rate[generator] == 0:
            if t != m.Time.last():
                return (m.P_MIN[generator] * (
                            m.Bin_Generator_On[generator, t] + m.Bin_Startup_Indicator[generator, t + 1])) + \
                       m.Generator_Power_Output_Above_Pmin[generator, t]
            else:
                return (m.P_MIN[generator] * m.Bin_Generator_On[generator, t]) + m.Generator_Power_Output_Above_Pmin[
                    generator, t]
        else:
            # Startup trajectory
            Startup_Traj = {i + 1: i * (m.P_MIN[generator] / m.Startup_Ramp_Rate[generator]) for i in
                            range(0, int(m.Startup_Ramp_Rate[generator]) + 1)}

            # x index
            x_index = [i for i in range(1, int(m.Startup_Ramp_Rate[generator]) + 1) if
                       ((t - i + m.Startup_Ramp_Rate[generator] + 2) <= m.Time.last()) and (
                                   (t - i + m.Startup_Ramp_Rate[generator] + 2) >= m.Time.first())]

            # Shutdown trajectory
            Shutdown_Traj = {i + 1: m.P_MIN[generator] - (m.P_MIN[generator] / m.Shutdown_Ramp_Rate[generator]) * i for
                             i in range(0, int(m.Shutdown_Ramp_Rate[generator]) + 1)}

            # y index
            y_index = [i for i in range(2, int(m.Shutdown_Ramp_Rate[generator]) + 2) if
                       ((t - i + 2) <= m.Time.last()) and ((t - i + 2) >= m.Time.first())]

            if t != m.Time.last():
                return (m.P_MIN[generator] * (
                            m.Bin_Generator_On[generator, t] + m.Bin_Startup_Indicator[generator, t + 1]) +
                        m.Generator_Power_Output_Above_Pmin[generator, t]
                        + sum(Startup_Traj[x] * m.Bin_Startup_Indicator[
                            generator, t - x + int(m.Startup_Ramp_Rate[generator]) + 2] for x in x_index)
                        + sum(Shutdown_Traj[y] * m.Bin_Shutdown_Indicator[generator, t - y + 2] for y in y_index))
            else:
                return ((m.P_MIN[generator] * m.Bin_Generator_On[generator, t]) + m.Generator_Power_Output_Above_Pmin[
                    generator, t]
                        + sum(Startup_Traj[x] * m.Bin_Startup_Indicator[
                            generator, t - x + int(m.Startup_Ramp_Rate[generator]) + 2] for x in x_index)
                        + sum(Shutdown_Traj[y] * m.Bin_Shutdown_Indicator[generator, t - y + 2] for y in y_index))

    m.Generator_Power_Output = Expression(m.Generators, m.Time, rule=Generator_Power_Output_rule)

    # Energy output for each generator
    def Energy_Output_Rule(m, generator, t):
        if t != m.Time.first():
            return ((m.Generator_Power_Output[generator, t - 1] + m.Generator_Power_Output[generator, t]) / 2) * m.DELTA
        else:
            return m.Generator_Power_Output[generator, t]

    m.Generator_Dispatch_Above_Pmin = Expression(m.Generators, m.Time, rule=Energy_Output_Rule)

    # Constraints
    # -----------
    # Power balance for each NEM zone

    def power_balance_rule(m, NEM_zone, t):
        Generators_in_Zone = [generator for generator in df_rm.loc[NEM_zone, 'DUID'] if
                              generator in m.Generators or m.HYDRO]
        if Generators_in_Zone:
            return (sum(m.Generator_Power_Output[generator, t] for generator in Generators_in_Zone if
                        generator in m.Generators) - m.Zone_Demand[NEM_zone, t] + m.Zone_Wind_Power_Variable[
                        NEM_zone, t] + sum(
                m.HYDRO_OUTPUT[hydro, t] for hydro in Generators_in_Zone if hydro in m.HYDRO)
                    == sum(m.AC_Transmission_Power_Flow[ac_line, t] * m.AC_C[ac_line, NEM_zone] for ac_line in
                           m.AC_Lines) + sum(
                        m.p_hvdc[dc_line, t] * m.HVDC_C[dc_line, NEM_zone] for dc_line in m.HVDC_Lines))
        else:
            return m.Zone_Demand[NEM_zone, t] == - sum(
                m.AC_Transmission_Power_Flow[ac_line, t] * m.AC_C[ac_line, NEM_zone] for ac_line in m.AC_Lines) - sum(
                m.p_hvdc[dc_line, t] * m.HVDC_C[dc_line, NEM_zone] for dc_line in m.HVDC_Lines)

    m.power_balance = Constraint(m.NEM_Zones, m.Time, rule=power_balance_rule)

    # Limit flows over Heywood interconnector
    def heywood_rule(m, t):
        return -500 <= m.AC_Transmission_Power_Flow['MEL,SESA', t] <= 600

    m.heywood = Constraint(m.Time, rule=heywood_rule)

    # Limit flows over QNI interconnector
    def QNI_rule(m, t):
        return -1078 <= m.AC_Transmission_Power_Flow['NNS,SWQ', t] <= 600

    m.QNI = Constraint(m.Time, rule=QNI_rule)

    # Limit flow over Terranora interconnector
    def terranora_rule(m, t):
        return -210 <= m.AC_Transmission_Power_Flow['NNS,SEQ', t] <= 107

    m.terranora = Constraint(m.Time, rule=terranora_rule)

    # Limit interconnector flows between VIC and NSW
    def VIC_to_NSW_rule(m, t):
        return -1350 <= sum(
            m.AC_Transmission_Power_Flow[line, t] for line in ['CVIC,SWNSW', 'NVIC,SWNSW', 'NVIC,CAN']) <= 1600

    m.VIC_to_NSW = Constraint(m.Time, rule=VIC_to_NSW_rule)

    # DUIDs allocated to each region
    df_region_duids = df_g.reset_index().groupby('NEM_REGION')['DUID'].aggregate(lambda x: set(x))

    # Ensure upward reserve for each NEM region is maintained
    def reserve_up_rule(m, region, t):
        return sum(m.Generator_Reserve_Up[generator, t] for generator in df_region_duids.loc[region] if
                   generator in m.Generators) >= m.Min_Zone_Reserve_Up[region]

    m.reserve_up = Constraint(m.NEM_Regions, m.Time, rule=reserve_up_rule)

    # Ensure downward reserve for each NEM region is satisfied
    def reserve_down_rule(m, region, t):
        return sum(m.Generator_Reserve_Down[generator, t] for generator in df_region_duids.loc[region] if
                   generator in m.Generators) >= m.Min_Zone_Reserve_Down[region]

    m.reserve_down = Constraint(m.NEM_Regions, m.Time, rule=reserve_down_rule)

    # Use SCADA data to initialise 'on' state for first period
    def Initial_Gen_On_State_rule(m, generator):
        if df_scada.loc[t_dict[m.Time.first()], generator] > 1:
            return 1
        else:
            return 0

    m.Initial_Gen_On_State = Param(m.Generators, initialize=Initial_Gen_On_State_rule)

    # Logic describing relationship between generator on state, startup state, and shutdown state
    def logic_rule(m, generator, t):
        if t != m.Time.first():
            return m.Bin_Generator_On[generator, t] - m.Bin_Generator_On[generator, t - 1] == m.Bin_Startup_Indicator[
                generator, t] - m.Bin_Shutdown_Indicator[generator, t]
        else:
            return m.Bin_Generator_On[generator, t] - m.Initial_Gen_On_State[generator] == m.Bin_Startup_Indicator[generator, t] - \
                   m.Bin_Shutdown_Indicator[generator, t]

    m.logic = Constraint(m.Generators, m.Time, rule=logic_rule)

    # Minimum on time (in time intervals)
    def min_on_time_rule(m, generator, t):
        i_index = [i for i in range(t - int(m.TU[generator]) + 1, t + 1)]
        if t < m.TU[generator]:
            return Constraint.Skip
        else:
            return sum(m.Bin_Startup_Indicator[generator, i] for i in i_index) <= m.Bin_Generator_On[generator, t]

    m.min_on_time = Constraint(m.Generators, m.Time, rule=min_on_time_rule)

    # Minimum off time (in time intervals)
    def min_off_time_rule(m, generator, t):
        i_index = [i for i in range(t - int(m.Min_Off_Time[generator]) + 1, t + 1)]
        if t < m.Min_Off_Time[generator]:
            return Constraint.Skip
        else:
            return sum(m.Bin_Shutdown_Indicator[generator, i] for i in i_index) <= 1 - m.Bin_Generator_On[generator, t]

    m.min_off_time = Constraint(m.Generators, m.Time, rule=min_off_time_rule)

    # Power output considering upward reserves
    def power_output_reserve_up_rule(m, generator, t):
        if t != m.Time.last():
            return m.Generator_Power_Output_Above_Pmin[generator, t] + m.Generator_Reserve_Up[generator, t] <= (
                        (m.P_MAX[generator] - m.P_MIN[generator]) * m.Bin_Generator_On[generator, t]
                        - (m.P_MAX[generator] - m.Gen_Shutdown_Capability[generator]) * m.Bin_Shutdown_Indicator[
                            generator, t + 1]
                        + (m.Gen_Startup_Capability[generator] - m.P_MIN[generator]) * m.Bin_Startup_Indicator[
                            generator, t + 1])
        else:
            return m.Generator_Power_Output_Above_Pmin[generator, t] + m.Generator_Reserve_Up[generator, t] <= (
                        m.P_MAX[generator] - m.P_MIN[generator]) * m.Bin_Generator_On[generator, t]

    m.power_output_reserve_up = Constraint(m.Generators, m.Time, rule=power_output_reserve_up_rule)

    # Power output considering downward reserves
    def power_output_reserve_down_rule(m, generator, t):
        return m.Generator_Power_Output_Above_Pmin[generator, t] - m.Generator_Reserve_Down[generator, t] >= 0

    m.power_output_reserve_down = Constraint(m.Generators, m.Time, rule=power_output_reserve_down_rule)

    # Ramp up limit
    def ramp_up_rule(m, generator, t):
        if t == m.Time.first():
            return Constraint.Skip
        else:
            return (m.Generator_Power_Output_Above_Pmin[generator, t] + m.Generator_Reserve_Up[generator, t]) - \
                   m.Generator_Power_Output_Above_Pmin[generator, t - 1] <= m.Ramp_UP[generator]

    m.ramp_up = Constraint(m.Generators, m.Time, rule=ramp_up_rule)

    # Ramp down limit
    def ramp_down_rule(m, generator, t):
        if t == m.Time.first():
            return Constraint.Skip
        else:
            return -(m.Generator_Power_Output_Above_Pmin[generator, t] - m.Generator_Reserve_Down[generator, t]) + \
                   m.Generator_Power_Output_Above_Pmin[generator, t - 1] <= m.Ramp_Down[generator]

    m.ramp_down = Constraint(m.Generators, m.Time, rule=ramp_down_rule)

    # Wind output limit (allows curtailment if intermittent generation is too high)
    def wind_output_rule(m, NEM_zone, t):
        return m.Zone_Wind_Power_Variable[NEM_zone, t] <= m.Zone_Wind_Power_Parameter[NEM_zone, t]

    m.wind_output = Constraint(m.NEM_Zones, m.Time, rule=wind_output_rule)

    # If shutdown ramp is larger than Pmax, m.Generator_Power_Output_Above_Pmin[generator,t] + m.Generator_Reserve_Up[generator,t] may be greater than P_MAX[generator]
    # Need this constraint to ensure power output is correctly constrained.
    def max_power_output_rule(m, generator, t):
        return m.Generator_Power_Output_Above_Pmin[generator, t] + m.P_MIN[generator] + m.Generator_Reserve_Up[
            generator, t] <= m.P_MAX[generator]

    m.max_power_output_rule = Constraint(m.Generators, m.Time, rule=max_power_output_rule)

    # Absolute flow over AC links
    def abs_ac_flow_up_rule(m, ac_line, t):
        return m.Power_AC_Interconnector_up[ac_line, t] >= m.AC_Transmission_Power_Flow[ac_line, t]

    m.abs_ac_flow_up = Constraint(m.AC_Lines, m.Time, rule=abs_ac_flow_up_rule)

    def abs_ac_flow_lo_rule(m, ac_line, t):
        return m.Power_AC_Interconnector_lo[ac_line, t] >= - m.AC_Transmission_Power_Flow[ac_line, t]

    m.abs_ac_flow_lo = Constraint(m.AC_Lines, m.Time, rule=abs_ac_flow_lo_rule)

    # Absolute flow over HVDC links
    def abs_hvdc_flow_up_rule(m, dc_line, t):
        return m.p_hvdc_up[dc_line, t] >= m.p_hvdc[dc_line, t]

    m.abs_hvdc_flow_up = Constraint(m.HVDC_Lines, m.Time, rule=abs_hvdc_flow_up_rule)

    def abs_hvdc_flow_lo_rule(m, dc_line, t):
        return m.p_hvdc_lo[dc_line, t] >= - m.p_hvdc[dc_line, t]

    m.abs_hvdc_flow_lo = Constraint(m.HVDC_Lines, m.Time, rule=abs_hvdc_flow_lo_rule)

    # Objective function
    # ------------------
    # Minimise total cost of generation over the time horizon

    def objective_rule(m):
        return sum((m.SRMC[generator] * m.Generator_Dispatch_Above_Pmin[generator, t]) + (
                    m.Startup_Cost[generator] * m.Bin_Startup_Indicator[generator, t]) for generator in m.Generators for
                   t in m.Time) +\
               sum(5 * (m.Power_AC_Interconnector_up[ac_line, t] + m.Power_AC_Interconnector_lo[ac_line, t]) for ac_line in
            m.AC_Lines for t in m.Time) + sum(
            5 * (m.p_hvdc_up[dc_line, t] + m.p_hvdc_lo[dc_line, t]) for dc_line in m.HVDC_Lines for t in m.Time)

    m.objective = Objective(rule=objective_rule, sense=minimize)

    # Setup solver
    # ------------
    solver = 'gurobi'
    solver_io = 'lp'
    stream_solver = True
    keepfiles = True
    m.dual = Suffix(direction=Suffix.IMPORT)
    opt = SolverFactory(solver, solver_io=solver_io)
    # opt.options['MIPGap'] = 2e-3
    opt.options['TimeLimit'] = 3600

    # Solve model
    results_initial = opt.solve(m, keepfiles=keepfiles, tee=stream_solver)

    # Fix integer variables
    for generator in m.Generators:
        for t in m.Time:
            m.Bin_Generator_On[generator, t].fix()
            m.Bin_Startup_Indicator[generator, t].fix()
            m.Bin_Shutdown_Indicator[generator, t].fix()

    # Re-solve to obtain dual of power balance constraints
    results_final = opt.solve(m, keepfiles=keepfiles, tee=stream_solver)

    # Store all instance solutions in ac_line results object
    m.solutions.store_to(results_final)

    # Retrieve total power and energy output values
    p_hat = []
    e_output = []
    for generator in m.Generators:
        for t in m.Time:
            p_hat.append((generator, t, t_dict[t], value(m.Generator_Power_Output[generator, t])))
            e_output.append((generator, t, t_dict[t], value(m.Generator_Dispatch_Above_Pmin[generator, t])))

    # Dataframe for total power output from each unit for each time interval
    df_p_hat = pd.DataFrame(data=p_hat, columns=['DUID', 'T_INDEX', 'T_STAMP', 'VALUE']).pivot(index='T_STAMP',
                                                                                               columns='DUID',
                                                                                               values='VALUE')

    # Dataframe for total energy output from each unit for each time interval
    df_e_output = pd.DataFrame(data=e_output, columns=['DUID', 'T_INDEX', 'T_STAMP', 'VALUE']).pivot(index='T_STAMP',
                                                                                                     columns='DUID',
                                                                                                     values='VALUE')

    # Store dataframes in results dictionary
    results_final['df_p_hat'] = df_p_hat
    results_final['df_e_output'] = df_e_output
    results_final['t_dict'] = t_dict

    # Save to file
    with open(os.path.join(output_dir, fname), 'wb') as f:
        pickle.dump(results_final, f)

    return m


# Model with hydro output determined by UC
m = run_uc_model('uc_results.pickle', fix_hydro=False)

# Model with hydro output fixed to SCADA output values
# m_fixed_hydro = run_uc_model('uc_fixed_hydro_results.pickle', fix_hydro=True)