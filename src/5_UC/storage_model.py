

# Plotly plotting library initialisation for offline & notebook use
import numpy as np

from dask.distributed import Client, progress, get_client, secede, rejoin

import plotly.offline as py
import os
import pandas as pd
import numpy as np
import networkx as nx
# ## Declare paths to files
# Core data directory
data_dir = os.path.abspath(os.path.join(os.path.curdir, os.path.pardir, os.path.pardir, 'data'))

# Network directory
network_dir = os.path.abspath(os.path.join(os.path.curdir, os.path.pardir, '1_network'))

# Generators directory
gens_dir = os.path.abspath(os.path.join(os.path.curdir, os.path.pardir, '2_generators'))

# Signals directory
signals_dir = os.path.abspath(os.path.join(os.path.curdir, os.path.pardir, '3_load_and_dispatch_signals'))

# Output path
output_dir = os.path.abspath(os.path.join(os.path.curdir, 'output'))
regions=['SA','TAS','NSW','QLD','VIC']
regions1=['SA1','TAS1','NSW1','QLD1','VIC1']

def get_ESOO_Renewables(zone, ref_year=2018):
    '''
    This code saves the REZ data for one year. If solar or wind is not in zone, np.nan's are returned.
    '''
    # Ref year is fiscal start

    region = zone['NEM Region'][0]
    file_name = zone['File_Name']
    zone_name = zone['Name']
    year = ref_year
    ref_year = str(ref_year)[-2:] + str(ref_year + 1)[-2:]
    # load_solar:
    if zone.Solar == 1:
        type_ = 'Solar'
        folder = 'NEM_data/REZ_ESOO/2018 REZ ' + type_ + ' Traces'

        file = folder + '/' + file_name + '_' + ref_year + '.csv'
        solar_data = load_REZ_file(file, year, type_, zone)
    else:
        type_ = 'Solar'
        file = 'NEM_data/REZ_ESOO/2018 REZ Solar Traces/Broken Hill_1011.csv'
        solar_data = load_REZ_file(file, year, type_, zone) * np.nan
    ref_year = 'RY' + str(year)[-2:]
    # load_wind
    if zone.Wind == 1:
        type_ = 'Wind'
        folder = 'NEM_data/REZ_ESOO/2018 REZ ' + type_ + ' Traces'
        file = folder + '/' + file_name + '_' + ref_year + '.csv'
        wind_data = load_REZ_file(file, year, type_, zone)
    else:
        type_ = 'Wind'
        file = 'NEM_data/REZ_ESOO/2018 REZ Solar Traces/Broken Hill_1011.csv'
        wind_data = load_REZ_file(file, year, type_, zone) * np.nan
    data = pd.concat([wind_data, solar_data], axis=1)
    data.to_parquet('NEM_data/REZ_ESOO/Cached_Data/' + zone_name + '_' + str(year) + '.parquet')
    return data


def run_ESOO_renewables():
    '''
    Standard run to load
    '''
    zones = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                          sheet_name='Renewable Energy Zones (2)')  # .Name.values
    zones['region'] = zones['NEM Region'].str[0]
    types = ['Solar', 'Wind']
    ref_years = [2012, 2013, 2014, 2015, 2016, 2017]
    regions = ['SA', 'TAS', 'NSW', 'QLD', 'VIC']
    zones = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx', sheet_name='Renewable Energy Zones (2)')

    futures = [client.submit(get_ESOO_Renewables, zone, ref_year) for ref_year in ref_years for index, zone in
               zones.iterrows()]
    progress(futures, notebook=False)

    return futures


def rename_rez_files():
    folder = "NEM_data/REZ_ESOO/2018 REZ Solar Traces/"

    for filename in os.listdir(folder):
        try:
            dst = filename.split('_')[1] + '_' + filename.split('_')[2]
            src = folder + filename
            dst = folder + dst

            # rename() function will
            # rename all the files
            os.rename(src, dst)
        except:
            print(filename)
def get_new_dispatchable_generation(year,df_g,Buid_cost):
    '''
    This fuction returns a list of new build dispatchable generation and minimum unit size
    '''
    parameters=pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                          sheet_name='New_Generator_Parameters',index_col=0)

    heat_rates={'Black Coal':8.98,'OCGT':11.75,'CCGT':7.58}#,'Biomass':13.39}#GJ/MWh
    all_new_generators=[]

    for heat_rate in heat_rates:
        new_generators=[]

        if heat_rate=='Biomass':
            new_generators=df_g.groupby('NEM_ZONE').first()[['Fuel_cost_Forecast ($/GJ)']]
            new_generators['Fuel_cost_Forecast ($/GJ)']=0.53
            new_generators['build_cost']=Buid_cost.loc[year]['Biomass'].iloc[0]


        elif heat_rate=='Black Coal':
            new_generators=df_g[df_g.FUEL_TYPE=='Black coal'].groupby('NEM_ZONE').mean()[['Fuel_cost_Forecast ($/GJ)','RR_UP','RR_DOWN']]
            new_generators['build_cost']=Buid_cost.loc[year]['Black Coal (supercritical PC)'].iloc[0]
            new_generators['unit_size']=1000
            new_generators['FUEL_TYPE']='Black coal'
            new_generators['MIN_GEN'] = parameters.loc[heat_rate, 'MIN_GEN']
            new_generators['Lifetime'] = 40
        else:
            new_generators=df_g[df_g.FUEL_TYPE=='Natural Gas (Pipeline)'].groupby('NEM_ZONE').mean()[['Fuel_cost_Forecast ($/GJ)']]
            new_generators['build_cost']=Buid_cost.loc[year][heat_rate].iloc[0]
            new_generators['FUEL_TYPE'] = 'Natural Gas (Pipeline)'
            new_generators['Lifetime'] = 30
            if heat_rate=='OCGT':
                new_generators['unit_size']=150
            elif heat_rate=='CCGT':
                new_generators['unit_size']=300

        new_generators['type']=heat_rate
        new_generators['heat_rate']=heat_rates[heat_rate]
        new_generators['Heat rate (GJ/MWh)'] = heat_rates[heat_rate]
        new_generators['VOM'] = parameters.loc[heat_rate, 'VOM']
        new_generators['SRMC_ISP']=new_generators['heat_rate']*new_generators['Fuel_cost_Forecast ($/GJ)']+new_generators['VOM']
        new_generators['RR_UP'] = parameters.loc[heat_rate, 'RR_UP']
        new_generators['RR_DOWN'] = parameters.loc[heat_rate, 'RR_DOWN']
        new_generators['standard_priced_capacity'] = parameters.loc[heat_rate, 'standard_priced_capacity']
        new_generators['REG_CAP']=1


        all_new_generators.append(new_generators)

    all_new_generators=pd.concat(all_new_generators, sort=True)
    all_new_generators['name'] = all_new_generators.index + ('_' + all_new_generators.type)
    all_new_generators = all_new_generators.reset_index().set_index('name')
    all_new_generators['Installed'] = False
    all_new_generators['FUEL_CAT'] = 'Fossil'
    all_new_generators['SCHEDULE_TYPE'] = 'SCHEDULED'
    all_new_generators['MIN_ON_TIME'] = 1
    return all_new_generators

def get_REZ_lists(year=2018):
    '''

    This function returns a list of REZ and the zone build cost for PV and Wind

    '''
    zones = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                          sheet_name='Renewable Energy Zones (2)')  # .Name.values
    zones_wind = zones.loc[zones.Wind == 1].set_index('Name')  # ['Name'].groupby(zones['NEM Zone']).apply(list)
    zones_solar = zones.loc[zones.Solar == 1].set_index('Name')  # ['Name'].groupby(zones['NEM Zone']).apply(list)
    zones_css = zones.loc[zones.Solar == 1].set_index('Name')  # ['Name'].groupby(zones['NEM Zone']).apply(list)

    Build_cost = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                               sheet_name='Generator Regional Build co (2')  # .Name.values

    Build_cost = Build_cost.set_index('Generator Name')
    Build_cost_wind = Build_cost.loc[Build_cost['Technology Type '] == 'Wind']
    Build_cost_wind.columns = Build_cost_wind.columns.str[:4]
    zones_wind['wind_build_cost'] = Build_cost_wind[str(year)]

    Build_cost_solar = Build_cost.loc[Build_cost['Technology Type '] == 'Large scale Solar PV']
    Build_cost_solar.columns = Build_cost_solar.columns.str[:4]

    # Build_cost_solar.loc['South West New South Wales']=Build_cost_solar.loc['Riverland (NSW)']

    zones_solar = zones_solar.sort_index()
    zones_solar['solar_build_cost'] = Build_cost_solar[str(year)].sort_index()

    Build_cost_css = Build_cost.loc[Build_cost['Technology Type '] == 'Solar Thermal 8hrs']
    Build_cost_css.columns = Build_cost_css.columns.str[:4]

    # Build_cost_css.loc['South West New South Wales']=Build_cost_css.loc['Riverland (NSW)']

    zones_css = zones_css.sort_index()
    zones_css['css_build_cost'] = Build_cost_css[str(year)].sort_index()

    return zones_solar, zones_wind,zones_css



def fiscal_year_start(dt):
    # dt=dt.index
    if dt.month <= 6:
        return dt.year - 1
    else:
        return dt.year


def get_REZ_data(zone, ref_year):
    '''Returns the zone profiles for REZ'''
    return pd.read_parquet('NEM_data/REZ_ESOO/Cached_Data/' + zone + '_' + str(ref_year) + '.parquet')


def save_all_zone_files(client):
    '''
    This saves the data for the REZ to one dataframe and distributes this by simulation year
    '''
    ref_years = [2012, 2013, 2014, 2015, 2016, 2017]
    client.restart()
    zones = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                          sheet_name='Renewable Energy Zones (2)',index_col=0)  # .Name.values
    zones_wind = zones.loc[zones.Wind == 1].set_index('Name')  # ['Name'].groupby(zones['NEM Zone']).apply(list)
    zones_solar = zones.loc[zones.Solar == 1].set_index('Name')  # ['Name'].groupby(zones['NEM Zone']).apply(list)

    for ref_year in ref_years:
        solar_profiles = []
        wind_profiles = []

        for zone in zones_solar.index:
            solar_profiles.append(get_REZ_data(zone, ref_year).Solar.to_frame(zone))
        for zone in zones_wind.index:
            wind_profiles.append(get_REZ_data(zone, ref_year).Wind.to_frame(zone))

        wind_profiles = pd.concat(wind_profiles, axis=1)
        solar_profiles = pd.concat(solar_profiles, axis=1)

        futures1 = [client.submit(group.to_parquet,
                                  'NEM_data/REZ_ESOO/Solar/Financial_Start_' + str(name) + '_REF_' + str(
                                      ref_year) + '.parquet') for name, group in
                    solar_profiles.groupby(solar_profiles.index.map(fiscal_year_start))]
        progress(futures1, notebook=False)
        futures2 = [client.submit(group.to_parquet, 'NEM_data/REZ_ESOO/Solar/Calandar_' + str(name) + '_REF_' + str(
            ref_year) + '.parquet') for name, group in
                    solar_profiles.groupby(solar_profiles.index.year)]
        progress(futures2, notebook=False)
        futures3 = [client.submit(group.to_parquet,
                                  'NEM_data/REZ_ESOO/Wind/Financial_Start_' + str(name) + '_REF_' + str(
                                      ref_year) + '.parquet') for name, group in
                    wind_profiles.groupby(wind_profiles.index.map(fiscal_year_start))]
        progress(futures3, notebook=False)
        futures4 = [client.submit(group.to_parquet,
                                  'NEM_data/REZ_ESOO/Wind/Calandar_' + str(name) + '_REF_' + str(ref_year) + '.parquet')
                    for name, group in wind_profiles.groupby(wind_profiles.index.year)]
        progress(futures4, notebook=False)


def get_REZ_profiles_all_zones(type_='Wind', year=2018, ref_year=2017, Calandar=False):
    '''
    This function gets the saved data for use in the model
    '''
    if Calandar == False:
        return pd.read_parquet('NEM_data/REZ_ESOO/' + type_ + '/Financial_Start_' + str(year) + '_REF_' + str(
            ref_year) + '.parquet').resample('h').mean().resample('30T').interpolate('linear')
    else:
        return pd.read_parquet(
            'NEM_data/REZ_ESOO/' + type_ + '/Calandar_' + str(year) + '_REF_' + str(ref_year) + '.parquet').resample(
            'h').mean().resample('30T').interpolate('linear')


def get_fuel_cost_forecast(year, Coal_price,Gas_price,Diesel=False):
    '''
    This returns the fuel cost forecast
    '''
    Price = pd.concat([Coal_price.loc[str(year)].transpose(), Gas_price.loc[str(year)].transpose()])
    Price.columns = ['Fuel_cost_Forecast ($/GJ)']
    Price = Price.rename(index={'Hallett': 'Hallett GT'})
    if Diesel:
        Price = Price.loc['Diesel_forecast'].values[0]
    else:
        Price = Price.drop('Diesel_forecast').sort_index()
    return Price


def get_build_cost_forecast(year, type_='Large Scale Battery Storage (2hrs storage)'):
    '''
    This returns the fuel cost forecast
    '''
    cost = Buid_cost.loc[str(year)][type_].values[0]
    return cost

def get_ESOO_Load(region='NSW',POE=10,ref_year=2018):
    ''''
    This returns the demand forecasts from the ESOO with POE = 50
    '''
    file='NEM_data/Load_ESOO/OPSO/N_ESOO18_OPSO_'+region+'_POE'+str(POE)+'_REFYEAR'+str(ref_year)+'.csv'
    data=pd.read_csv(file).set_index(['Year','Month','Day'])
    data.columns=data.columns.astype(int)

    data.stack().index.get_level_values(0)
    date={'year':data.stack().index.get_level_values(0),'month':data.stack().index.get_level_values(1), 'day':data.stack().index.get_level_values(2) ,'hour':data.stack().index.get_level_values(3)/2 }

    data=data.stack().reset_index()
    data.index=pd.to_datetime(pd.DataFrame(date))
    data=data[0]

    data.to_frame(name=region+'_'+str(POE)+'_'+str(ref_year)).to_parquet('NEM_data/Load_ESOO/load_profiles/'+region+'_'+str(ref_year)+'_'+str(POE)+'.parquet')
def run_ESOO_load():
    ref_years=[2016,2017,2018]
    regions=['SA','TAS','NSW','QLD','VIC']
    POEs=[10,50]
    futures=[client.submit(get_ESOO_Load,region, POE,ref_year) for region in regions for POE in POEs for ref_year in ref_years]

    return futures
def get_region_demand_forecast(region='NSW',POE=10,ref_year=2018):
        return pd.read_parquet('NEM_data/Load_ESOO/load_profiles/'+region+'_'+str(ref_year)+'_'+str(POE)+'.parquet')

def get_all_region_forecasts(regions=['NSW1', 'QLD1', 'SA1', 'TAS1', 'VIC1'],POE=10,year=2018,ref_year=2017,Cal_year=True):
    demand=pd.concat([get_region_demand_forecast(region=region[:-1],POE=POE,ref_year=ref_year).iloc[:,0] for region in regions],axis=1)
    demand.columns=regions
    if Cal_year==False:
        return demand.loc[demand.index.map(fiscal_year_start)==year]
    else:    
        return demand.loc[str(year)]


def get_storage_cost(year,Cal_year):
    '''
    Returns the storage costs from the 2019 ISP

    '''
    storage_cost = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                                 sheet_name='Storage Regional Build cost (2',index_col=0)
    battery_storage_cost = storage_cost.loc[storage_cost.type == 'Battery storage 2hrs'].set_index('Zone Name').iloc[:,
                           3:].transpose()
    battery_storage_cost.index = pd.to_datetime('1 July ' + battery_storage_cost.index.str[:4])


    PH_storage_cost = storage_cost.loc[storage_cost.type == 'Pumped hydro 6 hrs'].set_index('Zone Name').iloc[:,
                      3:].transpose()
    PH_storage_cost.index = pd.to_datetime('1 July ' + PH_storage_cost.index.str[:4])
    if Cal_year:
        battery_storage_cost = battery_storage_cost.resample('m').mean().interpolate('linear').resample('ys').mean()  # $/GJ
        PH_storage_cost = PH_storage_cost.resample('m').mean().interpolate('linear').resample('ys').mean()  # $/GJ

    battery_storage_cost = battery_storage_cost.loc[year]
    PH_storage_cost=PH_storage_cost.loc[year]

    return battery_storage_cost, PH_storage_cost


def remove_retired_gen(df_g, year,yallourn=False):
    '''
    Sets the parameters of generators as closed.
    '''
    if yallourn:
        df_g.loc["Yallourn 'W' Power Station",'Retirement date']=pd.to_datetime('1 July 2019')

    df_g.loc[df_g['Retirement date'].dt.year<=year,['REG_CAP','standard_priced_capacity','MIN_GEN']]=0

    print('Closed Plant (EOL):',end='')
    if len(df_g.loc[df_g.REG_CAP == 0].index)>0:
        print()
        for p in df_g.loc[df_g.REG_CAP == 0].index:
            print('\t- ' + p)
    else:
        print(' No generators at end of life')

    return df_g
# ##  Load data
def get_generator_availability(df_g_full,df_g,df_inter=None, ref_year='2017', Cal_year=True):
    '''
    This function returns the max and min generation for all installed generators. Using historical forced outages gives a
    more accurate estimation of capacity expansion required
    '''



    Min_gen_on = pd.DataFrame(index=df_inter.index, columns=df_g_full.index)
    Min_gen_on.loc[:, df_g_full.index] = df_g_full.MIN_GEN.values

    max_avil = pd.read_parquet('NEM_data/MP_Max_avil.parquet')
    max_avil['WHOE1'] = 0
    max_avil['WHOE2'] = 0
    max_avil.columns = max_avil.columns.str.replace('#', '')
    max_avil = max_avil.loc[~max_avil.index.duplicated(keep='first')]

    if Cal_year:
        max_avil = pd.concat([max_avil.loc['Jan ' + str(ref_year + 1):'June ' + str(ref_year + 1)],
                              max_avil.loc['July ' + str(ref_year):'December ' + str(ref_year)]], sort=False).iloc[:-1]
    else:
        int('Not implemented')

    max_av_date = df_inter[~((df_inter.index.month == 2) & (df_inter.index.day == 29))].index
    max_avil.index = max_av_date
    max_avil = max_avil.reindex(df_inter.index).ffill()

    max_avil = max_avil.reindex(Min_gen_on.index).bfill()
    max_avil = max_avil[Min_gen_on.columns]

    Min_gen_on[max_avil <= Min_gen_on] = max_avil[max_avil <= Min_gen_on]

    min_gen = []
    max_gen = []
    for name, DUIDS in df_g.DUIDS.to_frame().dropna().iterrows():
        # print(DUIDS)
        min_gen.append(Min_gen_on[list(DUIDS)[0]].sum(1).to_frame(name))
        max_gen.append(max_avil[list(DUIDS)[0]].sum(1).to_frame(name))

    min_gen = pd.concat(min_gen, axis=1)
    max_gen = pd.concat(max_gen, axis=1)

    max_gen.columns = max_gen.columns.str.title()
    min_gen.columns = min_gen.columns.str.title()

    return max_gen, min_gen

def get_new_renewable_profiles(New_Wind_Capacity=None,New_Solar_Capacity=None, year=2020, ref_year=2017,
                               Calandar=True, NEMZones=True,yallourn=False):
    '''
    This function returns a profile for installed generatino in each REZ
    new installed wind and solar are added to the REZs

    '''
    data_colums = ['TECH', 'MW', 'REZ', 'STATE']
    Registered_Generators = \
    pd.read_excel('NEM_data/New Wind and Solar Plant.xlsx', sheet_name='Registered Generators').set_index(
        'Station Name')[data_colums]
    Projects_completed = \
    pd.read_excel('NEM_data/New Wind and Solar Plant.xlsx', sheet_name='Projects completed').set_index('Station Name')[
        data_colums]
    Projects_under_construction = \
    pd.read_excel('NEM_data/New Wind and Solar Plant.xlsx', sheet_name='Projects under construction').set_index(
        'Station Name')[data_colums]
    All_Renwewables = pd.concat([Registered_Generators, Projects_completed, Projects_under_construction])
    All_Renwewables = All_Renwewables.loc[~All_Renwewables.index.duplicated(keep='first')]
    All_Renwewables = All_Renwewables.loc[All_Renwewables.REZ != '?']
    All_Renwewables.REZ = All_Renwewables.REZ.str.replace('\n', '')  # .str.replace('NSW','New South Wales')

    All_Renwewables.REZ = All_Renwewables.REZ.str.replace('Northern New South Wales Tablelands',
                                                          'Central West New South Wales')

    All_Renwewables.REZ = All_Renwewables.REZ.str.replace('Central New South Wales Tablelands', 'New England')
    # All_Renwewables.REZ=All_Renwewables.REZ.str.strip()
    # ll_Renwewables.REZ=All_Renwewables.REZ.str.replace(' SA',' South Australia')
    # l_Renwewables.REZ=All_Renwewables.REZ.str.replace(' QLD',' Queensland')
    # ll_Renwewables.REZ=All_Renwewables.REZ.str.replace('Mid-North South Australia','Mid-North South Australia ')
    # ll_Renwewables.REZ=All_Renwewables.REZ.str.replace(' Qld',' Queensland')
    All_Renwewables.TECH = All_Renwewables.TECH.str.replace('Hybrid', 'Wind')
    All_Renwewables = All_Renwewables.groupby(['TECH', 'REZ']).sum()

    Solar_Renwewables = All_Renwewables.loc['Solar']
    if isinstance(New_Solar_Capacity, pd.DataFrame):
        print('Adding '+str(New_Solar_Capacity.iloc[0].sum()+New_Wind_Capacity.iloc[0].sum())+' MW of renewable capacity from last simulation run')
        Solar_Renwewables = Solar_Renwewables.reindex(Solar_Renwewables.index).fillna(0)
        Solar_Renwewables['MW'] = Solar_Renwewables['MW'] + (New_Solar_Capacity.iloc[:, 0])

    profile = get_REZ_profiles_all_zones(type_='Solar', year=year, ref_year=ref_year, Calandar=Calandar)
    installed_solar_profiles = (Solar_Renwewables.MW * profile).fillna(0)

    Wind_Renwewables = All_Renwewables.loc['Wind']
    if isinstance(New_Wind_Capacity, pd.DataFrame):
        Wind_Renwewables = Wind_Renwewables.reindex(New_Wind_Capacity.index).fillna(0)
        Wind_Renwewables['MW'] = Wind_Renwewables['MW'] + (New_Wind_Capacity.iloc[:, 0])

    profile = get_REZ_profiles_all_zones(type_='Wind', year=year, ref_year=ref_year, Calandar=Calandar)
    installed_wind_profiles = (Wind_Renwewables.MW * profile).fillna(0)

    # The code below groups the REZ's to NEM zones

    zones = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                          sheet_name='Renewable Energy Zones (2)').set_index('Name')['NEM Zone']
#
    zones = zones.loc[~zones.index.duplicated(keep='first')]
    all_renewable_profiles = pd.concat([installed_wind_profiles, installed_solar_profiles], sort=True)
    all_renewable_profiles = all_renewable_profiles.groupby(all_renewable_profiles.index).sum()

    all_renewable_profiles.columns = all_renewable_profiles.columns.map(zones)

    all_renewable_profiles = all_renewable_profiles.groupby(all_renewable_profiles.columns, axis=1).sum()

    if NEMZones:
        return all_renewable_profiles
    else:
        return installed_wind_profiles, installed_solar_profiles


def get_unit_dispatch_data(year=2018,ref_year=2017, POE=50,Cal_year=False,
                 New_Wind_Capacity=None,New_Solar_Capacity=None,yallourn=False):
    # Network data
    # ------------
    # Nodes
    print('Collecting the '+str(year)+' datasets...\n')
    
    if (year==2018) and Cal_year:
        print('No full data for calandar year 2018 exists, setting year to 2019')
        year =2019
    df_n = pd.read_csv(os.path.join(network_dir, 'output', 'network_nodes.csv'), index_col='NODE_ID', dtype={'NEAREST_NODE':np.int32})

    # Edges
    df_e = pd.read_csv(os.path.join(network_dir, 'output', 'network_edges.csv'), index_col='LINE_ID')

    # HVDC links
    #df_hvdc = pd.read_csv(os.path.join(network_dir, 'output', 'network_hvdc_links.csv'), index_col='HVDC_LINK_ID')

    # AC interconnector links
    df_ac_i = pd.read_csv(os.path.join(network_dir, 'output', 'network_ac_interconnector_links.csv'), index_col='INTERCONNECTOR_ID')

    # AC interconnector limits
    df_ac_ilim = pd.read_csv(os.path.join(network_dir, 'output', 'network_ac_interconnector_flow_limits.csv'), index_col='INTERCONNECTOR_ID')

    # Power station - node assignments
    df_station_nodes = pd.read_csv(os.path.join(network_dir, 'output', 'network_power_stations-nodes.csv'), index_col='POWER_STATION_ID', dtype={'NEAREST_NODE':np.int32})
    Wind_REZ_Profiles=get_REZ_profiles_all_zones(type_='Wind', year=year, ref_year=ref_year, Calandar=Cal_year)
    Solar_REZ_Profiles=get_REZ_profiles_all_zones(type_='Solar', year=year, ref_year=ref_year, Calandar=Cal_year)
    solar_zones, wind_zones,css_zones=get_REZ_lists(year=year)

    # Generator data
    # --------------
    df_g = pd.read_csv(os.path.join(gens_dir, 'output', 'generators.csv'), index_col='DUID')
    df_g.index=df_g.index.str.replace('/','').str.replace('#','')
    df_g_full=df_g.copy()

    #df_g['DUIDS'] = df_g['STATIONNAME'].map(df_g.reset_index().groupby('STATIONNAME').DUID.apply(list))

    # Dispatch and load signals
    # -------------------------
    # Dispatch signals from SCADA data

    #old code: df_scada = pd.read_csv(os.path.join(signals_dir, 'output', 'signals_dispatch.csv'), index_col='SETTLEMENTDATE', parse_dates=['SETTLEMENTDATE'])
    df_scada=pd.read_parquet('NEM_data/2015 to 2019 Scada data_30T.parquet')


    if Cal_year:
        df_scada=df_scada.loc[str(ref_year)]
    else:
        df_scada = df_scada.loc[df_scada.index.map(fiscal_year_start)==ref_year]
    # Regional demand signals
    #old code: df_regd = pd.read_csv(os.path.join(signals_dir, 'output', 'signals_regional_load.csv'), index_col='SETTLEMENTDATE', parse_dates=['SETTLEMENTDATE'])
    #df_regd = pd.read_parquet('NEM_data/Regional_Demand_2018.parquet')
    df_regd = get_all_region_forecasts(year=year,ref_year=ref_year,Cal_year=Cal_year)

    # Cross-reference tables
    # ----------------------
    # AEMO DUIDs - Geoscience Australia power station names
    df_duid_stations = pd.read_csv(os.path.join(data_dir, 'cross_reference', 'DUID-GA-POWER_STATIONS.csv'), index_col='DUID')

    # ## Load Planning Studies Data
    #Load all useful ISP data
    source='NEM_data/2018 Integrated System Plan  Modelling Assumptions.xlsx'
    Inter_Proportioning_Factors=pd.read_excel(source,sheet_name='Proportioning factors',header=4,squeeze=True,skipfooter=3)
    #Build_cost=pd.read_excel(source,sheet_name='Build cost',header=6,skipfooter=13).reset_index().set_index('level_1').drop('level_0',1).transpose().dropna()
    #Build_cost.index=pd.to_datetime('1 July '+Build_cost.index.str[:4])
    Gas_price=pd.read_excel(source,sheet_name='Gas and Liquid fuel price (2)').set_index('Generator').transpose()
    Gas_price.index=pd.to_datetime('1 July '+Gas_price.index.str[:4])
    Coal_price=pd.read_excel(source,sheet_name='Coal and Biomass price',header=3,skipfooter=10).set_index('Generator').transpose().dropna()
    Coal_price.index=pd.to_datetime('1 July '+Coal_price.index.str[:4])
    

    Retirement=pd.read_excel(source,sheet_name = 'Retirement (2)',index_col='Generator')
    Retirement['Financial year beginning'] = pd.to_datetime('1 July '+Retirement['Financial year beginning'].astype(str))
    Retirement=Retirement.loc[~Retirement.index.duplicated(keep='first')]

    Auxiliary_Load = pd.read_excel(source,sheet_name='Auxiliary (2)',index_col='Generator')/100+1
    Emissions = pd.read_excel(source,sheet_name = 'Emissions (2)',index_col='Generator')
    Seasonal_ratings = pd.read_excel(source,sheet_name='Seasonal ratings (2)',index_col='Generator')
    Heat_rates = pd.read_excel(source,sheet_name='Heat rates (2)',index_col='Generator')
    #Buid_cost = pd.read_excel(source,sheet_name='Build cost (2)').set_index('Build cost ($/kW)').transpose()
    Buid_cost = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                              sheet_name='Build costs (2)').set_index(
        'Expected build costs - global temperature growth of 4 degrees').transpose()

    Buid_cost.index = pd.to_datetime('1 July ' + Buid_cost.index.str[:4])

    Storage_properties = pd.read_excel(source,sheet_name = 'Storage properties (2)',index_col='Property')
    Renewable_Policy = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',sheet_name='Renewable Policy Targets (2)').set_index('Unnamed: 0')
    Renewable_Policy.index=pd.to_datetime('1 July '+Renewable_Policy.index.str[:4])
    
    if Cal_year:
        Renewable_Policy=Renewable_Policy.resample('m').mean().interpolate('linear').resample('ys').mean()#$/GJ
        Buid_cost=Buid_cost.resample('m').mean().interpolate('linear').resample('ys').mean()#$/GJ
        Gas_price=Gas_price.resample('m').mean().interpolate('linear').resample('ys').mean()#$/GJ
        Coal_price=Coal_price.resample('m').mean().interpolate('linear').resample('ys').mean()#$/GJ
        #Build_cost=Build_cost.resample('m').mean().interpolate('linear').resample('ys').mean()#$/GJ

    # ## Organise model data
    # ### Summarise model data for each node
    # The steps taken to collate data used in the UC model are as follows:
    # 1. Initialise a dataframe, `df_m`, that will be used to summarise UC model data at each node
    # 2. Assign DUIDs to each node
    # 3. Create dataframe that contains intermittent power injections (from wind and solar) at each node for each time period.
    # 4. Create dataframe that contains demand for each NEM region for each time period
    # 5. Construct branch incidence matrix for the network


    # Dataframe that will contain a summary of data used in the DCOPF model
    df_m = df_n.copy()

    # DUIDs assigned to each node
    df_m['DUID'] = df_g.reset_index().groupby('NODE')[['DUID']].aggregate(lambda x: set(x)).reindex(df_m.index, fill_value=set())


    # ### Intermittent generation at each node
    # DUIDs corresponding to intermittent generators (wind and solar) are identified, and the net power injection at each node from these generators computed. Nodal power injections from intermittent sources are then aggregated by NEM zone.
    #
    # Note: Unit dispatch for some DUIDs is negative. This could be a result of the measurement methods used to collect dispatch data. As these negative values are small, they are unlikely to have a material impact on the final output of the model. However, the declaration of parameters in the UC model to follow may raise warnings when encountering negative values. To prevent this from occurring, these negative values are set to zero.

    # In[7]:


    # Find all intermittent DUIDs (wind and solar)
    mask = df_g['FUEL_CAT'].isin(['Wind', 'Solar'])
    ig_ids = df_g[mask].index
    if 0:
        # Find total intermittent generation at each node for each time period
        df_inter = df_scada.reindex(ig_ids, axis=1, fill_value=0).T.join(df_g[['NEM_ZONE']], how='left').groupby('NEM_ZONE').sum().T

        # Set negative dispatch values to 0
        mask = df_inter < 0
        df_inter[mask] = 0

        # Re-index, so all nodes are contained within columns
        df_inter = df_inter.reindex(df_n['NEM_ZONE'].unique(), axis=1, fill_value=0)
        df_inter.index = pd.to_datetime(df_inter.index)
        df_inter.index = df_inter.index + pd.Timedelta(days=365 * (year - ref_year))
    # ## Summarise model data for reduced network

    # Initialise dataframe that will contain model data for the reduced network
    df_rm = pd.DataFrame(index=df_m['NEM_ZONE'].unique())

    # Proportion of NEM region demand for each zone
    df_rm = df_m.groupby('NEM_ZONE')[['PROP_REG_D']].sum()

    # NEM region associated with each NEM zone
    df_rm['NEM_REGION'] = df_rm.apply(lambda x: df_m.drop_duplicates('NEM_ZONE').set_index('NEM_ZONE').loc[x.name , 'NEM_REGION'], axis=1)
    df_rm['DUID'] = df_g.reset_index().groupby('NEM_ZONE')['DUID'].aggregate(lambda x: set(x))


    # ### Demand for each NEM zone
    old_demand=False
    if old_demand==True:
        # Initialise matrix containing zonal demand
        df_zd = pd.DataFrame(index=df_rm.index, columns=df_regd.index)

        def get_zone_demand(row):
            # NEM region corresponding to NEM zone
            nem_region = df_m.drop_duplicates('NEM_ZONE').set_index('NEM_ZONE').loc[row.name, 'NEM_REGION']

            # Demand series for NEM zone's corresponding NEM region
            region_demand = df_regd.loc[:, nem_region]

            # Demand series for each NEM zone
            zone_demand = region_demand * df_rm.loc[row.name, 'PROP_REG_D']
            return zone_demand

        df_zd = df_zd.apply(get_zone_demand, axis=1).T
    else:
        # Rome Consulting disagregated demand
        zones_divider = pd.read_excel('NEM_data/Transmission_limits_Roam_Consulting_modified_for_ISP.xlsx',sheet_name='Zonal Load Distributions')[
            ['Region1', 'Zone Name', 'Peak_30', 'Offpeak_30']].set_index('Zone Name')
        df_zd = []
        for region, group in zones_divider.groupby('Region1'):
            region_zone_demand = []
            for zone, row in group.iterrows():
                index = df_regd.index
                # peak=df_regd[region].loc[(index.hour>17) & (index.hour<20)]
                peak = df_regd[region].loc[df_regd[region] >= df_regd[region].quantile(0.75)]
                off_peak = df_regd[region].drop(peak.index)
                region_zone_demand.append(
                    pd.concat([peak * row.Peak_30,
                               off_peak * row.Offpeak_30]).sort_index())
            df_zd.append(pd.concat(region_zone_demand, axis=1, keys=group.index))
        df_zd = pd.concat(df_zd, axis=1)




    # ### Incidence matrix for AC connections between NEM zones
    # Lines in the reduced network
    #rn_lines = ['NVIC,CVIC', 'ADE,NSA', 'SESA,ADE', 'MEL,SESA', 'MEL,CVIC', 'MEL,LV', 'MEL,NVIC', 'NVIC,SWNSW',
    #            'CAN,SWNSW', 'CAN,NCEN', 'NCEN,NNS', 'NNS,SWQ', 'SWQ,SEQ', 'SWQ,CQ', 'SEQ,CQ', 'CQ,NQ','TAS,LV', 'CVIC,NSA','NNS,SEQ']

    #rn_lines has AC and DC lines
    rn_lines = ['NVIC,CVIC', 'ADE,NSA', 'SESA,ADE', 'MEL,SESA', 'MEL,CVIC', 'MEL,LV', 'MEL,NVIC', 'NVIC,SWNSW',
                'CAN,SWNSW', 'CAN,NCEN', 'NCEN,NNS', 'NNS,SWQ', 'SWQ,SEQ', 'SWQ,CQ', 'SEQ,CQ', 'CQ,NQ', 'TAS,LV',
                'CVIC,NSA', 'NNS,SEQ']
    # Incidence matrix for network based on NEM zones as nodes
    df_ac_C = pd.DataFrame(index=rn_lines, columns=df_rm.index, data=0)

    for line in rn_lines:
        # Get 'from' and 'to' nodes for each line
        fn, tn = line.split(',')

        # Assign 'from' node value of 1
        df_ac_C.loc[line, fn] = 1

        # Assign 'to' node value of -1
        df_ac_C.loc[line, tn] = -1


    # ### Incidence matrix for HVDC links

    # Drop Directlink HVDC line (techincally not an interconnector)
    #df_hvdc = df_hvdc.drop('DIRECTLINK')

    # Assign 'from' and 'to' zones to each HVDC link
    #df_hvdc['FROM_ZONE'] = df_hvdc.apply(lambda x: df_n.loc[x['FROM_NODE'], 'NEM_ZONE'], axis=1)
    #df_hvdc['TO_ZONE'] = df_hvdc.apply(lambda x: df_n.loc[x['TO_NODE'], 'NEM_ZONE'], axis=1)

    # Incidence matrix for HVDC links
    #df_hvdc_C = pd.DataFrame(index=df_hvdc.index, columns=df_rm.index, data=0)

    #for index, row in df_hvdc.iterrows():
    #    # Extract 'from' and 'to' zones for each HVDC link
    #    fz, tz = row['FROM_ZONE'], row['TO_ZONE']


        # Assign value of 1 to 'from' zones
    #    df_hvdc_C.loc[index, fz] = 1

        # Assign value of -1 to 'to' zones
    #    df_hvdc_C.loc[index, tz] = -1

    # ## Determine the peak priced capacity

    df_g['standard_priced_capacity']=df_g['REG_CAP']

    for unit in (df_g.loc[df_g.FUEL_TYPE.isin(['Black coal','Brown coal'])]).index:
        scada=df_scada[unit.replace('/','').replace('#','')]
        scada=scada.loc[scada!=0]
        scada=scada.loc[(scada.index.hour>14)  & ( scada.index.hour<19)].mean()
        dif_cap=(df_g.loc[unit,'REG_CAP']-scada)
        if dif_cap<0:
            dif_cap=0
        dif_cap=df_g.loc[unit,'REG_CAP']-dif_cap
        df_g.loc[unit,'standard_priced_capacity']=df_scada[unit].quantile(0.9)

    df_g.loc[df_g['standard_priced_capacity']<0,'standard_priced_capacity']=0


    '''
    Group units by station
    '''

    #join Braemar to match ISP
    df_g['STATIONNAME']=df_g['STATIONNAME'].str.title()
    df_g.loc[df_g.STATIONNAME.isin(['Braemar 2 Power Station','Braemar Power Station']),'STATIONNAME']='Braemar Power Station'

    #Split torrens to match ISP
    df_g.loc[df_g.index.str.contains('TORRB'),'STATIONNAME']='Torrens Island B Power Station'
    df_g.loc[df_g.index.str.contains('TORRA'),'STATIONNAME']='Torrens Island A Power Station'

    df_g.loc['POR03','STATIONNAME'] ='Port Lincoln 3'
    df_g.loc['POR01','STATIONNAME'] ='Port Lincoln'

    df_scada_stations=df_scada[df_g.index].rename(columns=df_g['STATIONNAME']).groupby(lambda x:x, axis=1).sum()

    df_g['STATIONNAME']=df_g['STATIONNAME'].str.title()

    mean=[ 'NODE', 'EMISSIONS','MIN_GEN',
           'MIN_ON_TIME', 'MIN_OFF_TIME', 'SU_COST_COLD', 'SU_COST_WARM',
           'SU_COST_HOT', 'VOM', 'HEAT_RATE', 'NL_FUEL_CONS',
           'SRMC_2016-17','RR_STARTUP', 'RR_SHUTDOWN', 'RR_UP', 'RR_DOWN']

    sum_=[ 'FC_2016-17','REG_CAP','standard_priced_capacity']
    first=['STATIONNAME','NEM_REGION','NEM_ZONE','FUEL_TYPE', 'FUEL_CAT', 'SCHEDULE_TYPE']

    #Group Braemar station to match ISP

    df_g_stations=pd.concat([df_g.reset_index().groupby('STATIONNAME').DUID.apply(list).to_frame('DUIDS'),df_g[mean].groupby(df_g['STATIONNAME']).mean(),
              df_g[sum_].groupby(df_g['STATIONNAME']).sum(),
              df_g[first].groupby(df_g['STATIONNAME']).last()],axis=1)

    station_simulation=True
    if station_simulation==True:
        df_g=df_g_stations
        df_scada=df_scada_stations
        df_g.index.name='DUID'
        df_scada.index.name='DUID'
        ind=df_g.loc[df_g['REG_CAP']<df_g['standard_priced_capacity']].index
        df_g.loc[ind,'standard_priced_capacity']=df_g.loc[ind,'REG_CAP']
        def lists(lists):
            return { i  for i in lists }
        df_rm['DUID']=df_g.reset_index()['DUID'].groupby(df_g.reset_index()['NEM_ZONE']).apply(list).apply(lists)

        #set Hydro costs equal to 90% gas
        average_gas_SRMC=df_g.loc[(df_g['FUEL_TYPE']=='Natural Gas (Pipeline)') & (df_g['NEM_REGION']!='QLD1')]['SRMC_2016-17'].mean()
        df_g.loc[(df_g['FUEL_TYPE']=='Hydro')&(df_g['NEM_REGION']!='QLD1'),'SRMC_2016-17']=average_gas_SRMC*.9

    df_g.index=df_g.index.str.title()

    #select only SCHEDULED generators
    df_g=df_g.loc[(df_g['SCHEDULE_TYPE'] == 'SCHEDULED') & df_g['FUEL_CAT'].isin(['Hydro', 'Fossil']) & ~pd.isnull(
                df_g['MIN_ON_TIME'])].copy()

    #Match ISP station names with df_g
    df_g['ISP_Name']=np.nan
    for station in Seasonal_ratings.index:

        if station=='Mount Piper':
            station_lookup='Mt Piper'
        elif station=='Yabulu':
            station_lookup='Townsville'
        elif station=='Callide B':
            station_lookup='Callide Power Station'
        elif station=='Callide Power Plant':
            station_lookup='Callide C Nett Off'
        elif station=='Tarong':
            station_lookup='Tarong Power Station'
        elif station=='Tarong North':
            station_lookup='Tarong North'
        elif station=='Valley Power':
            station_lookup='Valley Power Peaking Facility'
        elif station=='Jeeralang A':
            station_lookup='Jeeralang "A"'
        elif station=='Jeeralang B':
            station_lookup='Jeeralang "B"'
        elif station=='Hallett GT':
            station_lookup='Hallett Power'
        elif station=='Tamar Valley CCGT':
            station_lookup='Valley Combined Cycle Power Station'
        elif station=='Mount Stuart':
            station_lookup='Mt Stuart Power Station'
        elif station=='Port Stanvac':
            station_lookup='Pt Stanvac Power Station'
        elif station=='Tumut 1':
            station_lookup='Tumut Power Station'
        elif station=='Murray 1':
            station_lookup='Murray'

        else:
            station_lookup=station.replace('GT','Gas Turbine')
        if False:#activate to check matching
            if len(df_g.loc[df_g.index.str.contains(station_lookup)].REG_CAP.values)==0:
                print(station)
                print(Seasonal_ratings.loc[station].values[1])
                print(df_g.loc[df_g.index.str.contains(station_lookup)].REG_CAP.values)
                print(df_g.loc[df_g.index.str.contains(station_lookup)].index.values)

        df_g.loc[df_g.index.str.contains(station_lookup),'ISP_Name']=station

        #remove vic hume station

    df_g=df_g.drop('Hume (Vic) Hydro Power Station')


    #add The ISP data to df_g
    df_g=df_g.reset_index().set_index('ISP_Name').sort_index()

    #Fill Seasonal_ratings
    df_g = pd.concat([df_g,Seasonal_ratings.sort_index()],axis=1,sort=False)#.lookup(Retirement)
    #Fill Auxiliary_Load
    df_g = pd.concat([df_g,Auxiliary_Load.sort_index()],axis=1,sort=False)#.lookup(Retirement)

    #Fill Emissions
    df_g = pd.concat([df_g,Emissions.sort_index()],axis=1,sort=False)

    #Fill in Fuel cost values
    df_g['Fuel_cost_Forecast ($/GJ)'] = np.nan
    df_g['Fuel_cost_Forecast ($/GJ)'] = (df_g['Fuel_cost_Forecast ($/GJ)']).fillna(get_fuel_cost_forecast(year, Coal_price,Gas_price,Diesel=False)['Fuel_cost_Forecast ($/GJ)'])

    #Fill in Heat rate values
    df_g['Heat rate (GJ/MWh)'] = np.nan
    df_g['Heat rate (GJ/MWh)'] = (df_g['Heat rate (GJ/MWh)']).fillna(Heat_rates['Heat rate (GJ/MWh)'])

    # Fill in Retirement
    df_g['Retirement date']=(df_g.index).map(Retirement['Financial year beginning'])
    df_g['Retirement date']=df_g['Retirement date'].fillna(df_g['Retirement date'].max())

    #set zero values for renewable energy
    df_g.loc[df_g.FUEL_TYPE.isin(['Solar','Hydro','Wind']),'Fuel_cost_Forecast ($/GJ)'] = 0
    df_g.loc[df_g.FUEL_TYPE.isin(['Solar','Hydro','Wind']),'Total emissions (kg/MWh)'] = 0
    df_g.loc[df_g.FUEL_TYPE.isin(['Solar','Hydro','Wind']),'Heat rate (GJ/MWh)'] = 0

    df_g = df_g.loc[:,~df_g.columns.duplicated()]

    #fix data for missing stations
    df_g.loc[df_g['Total emissions (kg/MWh)'].isna() & df_g.FUEL_TYPE.isin(['Diesel oil']),'Total emissions (kg/MWh)'] = 1445.858462 # taken from another diesel station
    df_g.loc[df_g['Total emissions (kg/MWh)'].isna() & df_g.FUEL_TYPE.isin(['Natural Gas (Pipeline)']),'Total emissions (kg/MWh)'] = 879.404541 # taken from another gas station


    df_g.loc[df_g['Fuel_cost_Forecast ($/GJ)'].isna() & df_g.FUEL_TYPE.isin(['Diesel oil']),'Fuel_cost_Forecast ($/GJ)']=get_fuel_cost_forecast(year, Coal_price,Gas_price,Diesel=True)
    df_g=df_g.loc[~df_g['Total emissions (kg/MWh)'].isna()]
    df_g=df_g.loc[~df_g['Fuel_cost_Forecast ($/GJ)'].isna()]
    df_g=df_g.loc[~df_g['Heat rate (GJ/MWh)'].isna()]
    df_g['SRMC_ISP'] = df_g['Heat rate (GJ/MWh)'] * df_g['Fuel_cost_Forecast ($/GJ)']+df_g['VOM']

    df_g.loc[df_g.FUEL_TYPE.isin(['Black coal','Brown coal'])][['SRMC_ISP','SRMC_2016-17']].sort_values('SRMC_ISP')
    df_g=df_g.reset_index().set_index('DUID')

    all_data=[]
    for fuel in ['Natural Gas (Pipeline)','Black coal', 'Brown coal']:
            Priceset=pd.read_parquet('NEM_data/Average_Priceset.parquet')
            stations=df_g.loc[df_g['FUEL_TYPE']==fuel]
            data=pd.concat([Priceset[[i for i in stations.index if i in Priceset.columns]].loc[2018],stations['SRMC_ISP']],axis=1,sort=True)#.iplot(mode='markers')
            data['fuel']=fuel
            all_data.append(data)
    all_data=pd.concat(all_data)

    all_data.to_excel('fuel_scatter.xlsx')

    # ## Minimum Reserve levels for each NEM region
    # Minimum reserve levels in MW for each NEM region are obtained from [4].

    battery_storage_cost, PH_storage_cost=get_storage_cost(year = str(year), Cal_year=Cal_year)
    transmission_limits=pd.read_excel('NEM_data/Transmission_limits_Roam_Consulting_modified_for_ISP.xlsx',sheet_name='transmission_flow_limits').set_index('Link')
    PH_Build_Limit = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                                   sheet_name='PH Build Limit').set_index('Location')
    Renewable_Build_Limit = pd.read_excel('NEM_data/2019 Input and Assumptions workbook.xlsx',
                                          sheet_name='Renewable Build limit').set_index('Location')
    new_dispatchable_generators=get_new_dispatchable_generation(year=str(year), df_g=df_g,Buid_cost=Buid_cost)

    df_mrl = pd.Series(data={'NSW1': 673.2, 'QLD1': 666.08, 'SA1': 195, 'TAS1': 194, 'VIC1': 498})

    #AEMO randomly adds a space at the end of the REZ names. :(
    wind_zones.index = wind_zones.index.str.strip()
    solar_zones.index = solar_zones.index.str.strip()
    Renewable_Build_Limit.index = Renewable_Build_Limit.index.str.strip()
    css_zones.index = css_zones.index.str.strip()
    Wind_REZ_Profiles.columns=Wind_REZ_Profiles.columns.str.strip()
    Solar_REZ_Profiles.columns = Solar_REZ_Profiles.columns.str.strip()
    PH_Build_Limit.index = PH_Build_Limit.index.str.strip()

    #create a list of new and existing generators:
    df_g['Installed'] = True

    df_g = pd.concat([df_g, new_dispatchable_generators], sort=True)
    df_g.index.name = 'DUID'

    #Adds total generation in for Hydro
    df_g.loc[df_g.FUEL_CAT == 'Hydro', 'Total_Hydro_Generation'] = df_scada[df_g.loc[df_g.FUEL_CAT == 'Hydro'].index].sum()

    df_g=remove_retired_gen(df_g, year,yallourn=yallourn)

    #get renewable profiles
    df_inter = get_new_renewable_profiles(New_Solar_Capacity=New_Solar_Capacity, New_Wind_Capacity=New_Wind_Capacity, year=year,
                                                                               ref_year=ref_year, Calandar=Cal_year)
    df_inter=df_inter.reindex(df_rm.index,axis=1).fillna(0)

    #gives the
    Generation_Maximum, Generation_Minimum=get_generator_availability(df_g=df_g,df_g_full=df_g_full,df_inter=df_inter, ref_year=ref_year, Cal_year=Cal_year)
    print('done!')
    return df_inter, df_mrl, df_station_nodes, df_n, df_g, df_g_full, df_regd, df_scada, df_ac_C, rn_lines,Renewable_Policy, df_zd,df_rm,solar_zones, wind_zones,css_zones,Wind_REZ_Profiles,Solar_REZ_Profiles,transmission_limits,Buid_cost,new_dispatchable_generators,battery_storage_cost, PH_storage_cost,Renewable_Build_Limit,PH_Build_Limit,Generation_Maximum, Generation_Minimum

