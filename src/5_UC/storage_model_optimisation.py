import pandas as pd
import numpy as np
from pyomo.environ import *

def present_value_scaler(lifetime, rate_of_return=0.05):
    try:
        lifetime=lifetime.value
        rate_of_return=rate_of_return.value
    except:
        a=1
    return sum([1/(1+rate_of_return)**n for n in range(0,lifetime)])


def create_model_parameters(m, simulate_storage, simulate_REZ,df_rm,wind_zones,solar_zones,df_ac_C,df_zd,days,start_days,df_g,Emissions_Limit,Wind_REZ_Profiles,Solar_REZ_Profiles,df_inter,df_mrl,
                            battery_storage_cost, PH_storage_cost,Renewable_Build_Limit,new_dispatchable_generators,Generation_Maximum,Generation_Minimum):
    '''
    This function creates all nessary model parameters
    '''
    #define Generators
    dispatchable_generators = (df_g['SCHEDULE_TYPE'] == 'SCHEDULED') & df_g['FUEL_CAT'].isin(
        ['Hydro', 'Fossil']) & ~pd.isnull(
        df_g['MIN_ON_TIME'])

    m.Generators = Set(initialize=df_g[dispatchable_generators].index)
    m.Coal_Generators=Set(initialize=list(df_g.loc[df_g.FUEL_TYPE.isin(['Black coal','Brown coal'])].index))

    m.Hydro_Generators = Set(initialize=df_g.loc[df_g.FUEL_CAT == 'Hydro'].index)

    # NEM zones
    m.NEM_Zones = Set(initialize=df_rm.index)

    # NEM Wind REZ
    m.Wind_REZ = Set(initialize=wind_zones.index)
    def new_wind_build_cost_build_cost_rule(m, REZ):
        return float(wind_zones.wind_build_cost.loc[REZ])
    m.Wind_Build_Cost = Param(m.Wind_REZ, initialize=new_wind_build_cost_build_cost_rule)

    # NEM solar REZ
    m.Solar_REZ = Set(initialize=solar_zones.index)
    def new_solar_build_cost_build_cost_rule(m, REZ):
        return float(solar_zones.solar_build_cost.loc[REZ])
    m.Solar_Build_Cost = Param(m.Solar_REZ, initialize=new_solar_build_cost_build_cost_rule)

    m.REZ = Set(initialize=Renewable_Build_Limit.index)

    #list of new dispatchable genertors
    m.new_dispatchable_generators = Set(initialize=new_dispatchable_generators.index)

    def new_dispatchable_generators_build_cost_rule(m, generator):
        return float(new_dispatchable_generators.loc[generator,'build_cost'])

    m.new_dispatchable_build_cost = Param(m.new_dispatchable_generators, initialize=new_dispatchable_generators_build_cost_rule)

    def new_dispatchable_generators_lifetime_rule(m, generator):
        return float(new_dispatchable_generators.loc[generator, 'Lifetime'])

    m.new_dispatchable_lifetime = Param(m.new_dispatchable_generators, initialize=new_dispatchable_generators_lifetime_rule)


    # NEM regions
    m.NEM_Regions = Set(initialize=df_rm['NEM_REGION'].unique())



    # AC lines
    m.AC_Lines = Set(initialize=df_ac_C.index)

    # (Dictionary to convert between timestamps and time indices)
    t_dict = {NEM_zone + 1: k for NEM_zone, k in enumerate(df_zd[start_days*48:start_days*48+days*48].index)}

    # Time indices
    m.Time = Set(initialize=list(t_dict.keys()), ordered=True)
    print('Creating Parameters...')
    
    # Parameters
    # ----------
    # Constant linear variable cost (SRMC)
    if simulate_storage:
        m.rate_of_return = Param(initialize=5/100)
        m.WACC = Param(initialize=6.25 / 100)
        #storage parameters
        a=1
        #Percentage of C Charge
        m.Battery_Power_Rate = Param(initialize=0.5)
        m.Battery_Round_Trip_Efficiency = Param(initialize=0.85)
        m.Cycle_Cost = Param(initialize=60)#$/MWh
        m.Battery_Life = Param(initialize=15)
        m.PH_Life = Param(initialize=50)

        m.PH_Round_Trip_Efficiency = Param(initialize=0.775)
        m.Wind_Life = Param(initialize=20)
        m.Solar_Life = Param(initialize=20)
        # Storage_costs:
        def Battery_cost_rule(m, NEM_zone):
            return float(battery_storage_cost[NEM_zone][0])

        m.Battery_Cost = Param(m.NEM_Zones, initialize=Battery_cost_rule)


        def PH_cost_rule(m, NEM_zone):
            return float(PH_storage_cost[NEM_zone][0])

        m.PH_Cost = Param(m.NEM_Zones, initialize=PH_cost_rule)

        #def PH_Max_Capacity_rule(m, NEM_zone):
        #    return float(10e6)

        #m.PH_Max_Capacity = Param(m.NEM_Zones, initialize=PH_Max_Capacity_rule)

    def Standard_Price_Capacity_rule(m, generator):
        return float(df_g.loc[generator, 'standard_priced_capacity'])

    m.Standard_Price_Capacity = Param(m.Generators, initialize=Standard_Price_Capacity_rule)
    
    if Emissions_Limit:
        def Generator_Emissions_rule(m, generator):
            return float(df_g.loc[generator, 'Total emissions (kg/MWh)'])

        m.Generator_Emissions_Rate = Param(m.Generators, initialize=Generator_Emissions_rule)
    
        
    def SRMC_rule(m, generator):
        return float(df_g.loc[generator, 'SRMC_ISP'])

    m.SRMC = Param(m.Generators, initialize=SRMC_rule)

    def generator_output_bound_rule(m, generator, t):
        if generator not in m.new_dispatchable_generators:
            return (float(Generation_Minimum.loc[t_dict[t],generator]), float(Generation_Maximum.loc[t_dict[t],generator]))
        else:
            a=1

    # Maximum power output
    def P_MAX_rule(m, generator, t):
        # if pre installed use historical availbility
        if generator not in m.new_dispatchable_generators:
            max = float(Generation_Maximum.loc[t_dict[t], generator])
        # if not installed use defined percentage
        else:
            max=float(df_g.loc[generator, 'REG_CAP'])
        return max

    m.P_MAX = Param(m.Generators, m.Time, initialize=P_MAX_rule)

    # Minimum power output
    def P_MIN_rule(m, generator, t):
        #if pre installed use historical availbility
        if generator not in m.new_dispatchable_generators:
            min_gen = float(Generation_Minimum.loc[t_dict[t], generator])
        # if not installed use defined percentage
        else:
            min_gen = df_g.loc[generator, 'MIN_GEN']
        # If no data for min gen as % of nameplate capacity, return 0
        if pd.isnull(min_gen):
            return float(0)
        else:
            return float(min_gen)

    m.P_MIN = Param(m.Generators,  m.Time,initialize=P_MIN_rule)

    # Time interval length in hours
    m.DELTA = Param(initialize=float(0.5))

    # Ramp up capability
    def RU_rule(m, generator):
        return float(df_g.loc[generator, 'RR_UP'] * m.DELTA.value)

    m.Ramp_UP = Param(m.Generators, initialize=RU_rule)

    # Ramp down capability
    def RD_rule(m, generator):
        return float(df_g.loc[generator, 'RR_DOWN'] * m.DELTA.value)

    m.Ramp_Down = Param(m.Generators, initialize=RD_rule)

    # Demand for each NEM zone
    def Demand_Zone_rule(m, NEM_zone, t):
        return float(df_zd.loc[t_dict[t], NEM_zone])

    m.Zone_Demand = Param(m.NEM_Zones, m.Time, initialize=Demand_Zone_rule)


    if simulate_REZ:
        # REZ Wind  for each NEM zone
        def Wind_REZ_rule(m, Wind_rez, t):
            return float(Wind_REZ_Profiles.loc[t_dict[t], Wind_rez])

        m.Wind_REZ_Profiles = Param(m.Wind_REZ, m.Time, initialize=Wind_REZ_rule)

        def Solar_REZ_rule(m, Solar_rez, t):
            return float(Solar_REZ_Profiles.loc[t_dict[t], Solar_rez])

        m.Solar_REZ_Profiles = Param(m.Solar_REZ, m.Time, initialize=Solar_REZ_rule)

        def REZ_solar_Bound_rule(m, REZ):
            return (float(0), float(Renewable_Build_Limit.loc[REZ, 'Medium Wind generation limits (MW)']))

        m.Maximum_Solar_Capacity = Param(m.Solar_REZ, initialize=REZ_solar_Bound_rule)
    # HVDC incidence matrix
    #def HVDC_C_rule(m, dc_line, NEM_zone):
    #    return float(df_hvdc_C.loc[dc_line, NEM_zone])

   #m.HVDC_C = Param(m.HVDC_Lines, m.NEM_Zones, initialize=HVDC_C_rule)

    # AC incidence matrix
    def AC_C_rule(m, ac_line, NEM_zone):
        return float(df_ac_C.loc[ac_line, NEM_zone])

    m.AC_C = Param(m.AC_Lines, m.NEM_Zones, initialize=AC_C_rule)

    # Intermittent generation for each zone
    def P_Renewable_rule(m, NEM_zone, t):
        wind_output = float(df_inter.loc[t_dict[t], NEM_zone])
        if wind_output < 0:
            return float(0)
        else:
            return wind_output

    m.Zone_Renewable_Power_Parameter = Param(m.NEM_Zones, m.Time, initialize=P_Renewable_rule)

    # Minimum reserve level (up) for each NEM zone
    def D_UP_rule(m, region):
        return float(df_mrl.loc[region])

    m.Min_Zone_Reserve_Up = Param(m.NEM_Regions, initialize=D_UP_rule)

    # Minimum reserve level (down) for each NEM region
    def D_DOWN_rule(m, region):
        return float(df_mrl.loc[region] / 10)

    m.Min_Zone_Reserve_Down = Param(m.NEM_Regions, initialize=D_DOWN_rule)

    return m

##########################################################################################
    
def create_model_variables(m, simulate_storage,simulate_REZ,df_zd,df_g,days,start_days, Emissions_Limit,simulate_PH,transmission_limits,PH_Build_Limit,Renewable_Build_Limit,df_rm,new_dispatchable_generators,
                           New_Dispatachable_Capacity,New_PH_Power_Capacity,New_Battery_Power_Capacity, Generation_Maximum,Generation_Minimum):
    print('Creating Model Variables...')
    # Variables
    # (Dictionary to convert between timestamps and time indices)
    t_dict = {NEM_zone + 1: k for NEM_zone, k in enumerate(df_zd[start_days * 48:start_days * 48 + days * 48].index)}

    #m.Bin_Shutdown_Indicator = Var(m.Coal_Generators, m.Time, within=Binary)
    if Emissions_Limit:
        m.CO2_Emissions = Var(m.Generators, m.Time, within=NonNegativeReals)#MW

    if simulate_storage:
        #storage parameters
        
        # The charge and discharge powers
        m.Battery_Discharge = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)#MW
        m.Battery_Charge = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)#MW
        
        #The level of battery in each zone
        m.Battery_Level = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)#MWh
        
        #battery capacity in each zone
        m.Battery_Capacity = Var(m.NEM_Zones, within=NonNegativeReals)#MWh

        def Installed_Battery_Bound(m, Zone):

            return (float(New_Battery_Power_Capacity.loc[Zone].values[0]), 1e10)

        if isinstance(New_Battery_Power_Capacity, pd.DataFrame):
            m.Battery_Power_Capacity = Var(m.NEM_Zones, within=NonNegativeReals, bounds=Installed_Battery_Bound)
        else:
            m.Battery_Power_Capacity = Var(m.NEM_Zones, within=NonNegativeReals)  # MW , bounds=(10,10)

        # Updated Demand with Storage 
        m.Zone_Demand_Storage = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)#MW

        # Fix bound for installed capacity
        def Installed_Dispatchable_Bound(m, generator):
            return ((New_Dispatachable_Capacity.loc[generator,'new_dispatchable_generator_Capacity']), 1e10)

        if isinstance(New_Dispatachable_Capacity, pd.DataFrame):
            m.new_dispatchable_generator_Capacity = Var(m.new_dispatchable_generators,
                                                        within=NonNegativeReals,bounds=Installed_Dispatchable_Bound)#MW
        else:
            m.new_dispatchable_generator_Capacity = Var(m.new_dispatchable_generators,
                                                        within=NonNegativeReals)  # ,bounds=(10,10))#MW

        #new generator capacity, bound to be greater then one unit ( unit size for Coal set to 1GW due to scales of infrastructure required for coal

        if simulate_PH:
            # storage parameters

            # The charge and discharge powers
            m.PH_Discharge = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)  # MW
            
            m.PH_Charge = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)  # MW


            # The level of battery in each zone
            m.PH_Level = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)  # MWh

            # battery capacity in each zone
            m.PH_Capacity = Var(m.NEM_Zones, within=NonNegativeReals)  # MWh

            # Updated Demand with Storage 
            m.PH_Zone_Demand_Storage = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)  # MW
            def Installed_PH_Bound(m, zone):

                return (New_PH_Power_Capacity.loc[zone,'PH_Power_Capacity'], 1e10)

            if isinstance(New_PH_Power_Capacity, pd.DataFrame):
                m.PH_Power_Capacity = Var(m.NEM_Zones, within=NonNegativeReals,bounds=Installed_PH_Bound)
            else:
                m.PH_Power_Capacity = Var(m.NEM_Zones, within=NonNegativeReals)  # MW , bounds=(10,10)

            def regional_PH_Bound_rule(m, region):
                return (float(0), float(PH_Build_Limit.loc[region[:-1], '6hrs storage']))


            m.Regional_PH_Capacity = Var(m.NEM_Regions,within=NonNegativeReals,bounds=regional_PH_Bound_rule)
    #

    # set a bound on the total generation from Hydro plants
    def Hydro_bound_rule(m, generator):
        return (0, float(df_g.loc[generator,'Total_Hydro_Generation']))

    m.Total_Hydro_Ouptut = Var(m.Hydro_Generators, bounds=Hydro_bound_rule, within=NonNegativeReals)

    # Reserve (up) for each generator
    m.Generator_Reserve_Up = Var(m.Generators, m.Time, within=NonNegativeReals)
    
    # The change in generator output per time period 
    m.Generator_Power_Change = Var(m.Generators, m.Time, within=Reals)
    
    # Resever (down) for each generator
    m.Generator_Reserve_Down = Var(m.Generators, m.Time, within=NonNegativeReals)

    # Wind power output at each node
    m.Zone_Renewable_Power_Variable = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)

    # Dispatch for each generator above P_MIN
    m.Generator_Power_Output_Above_Pmin = Var(m.Generators, m.Time, within=NonNegativeReals)

    #This was disabled because I am tired and infeasibilities suck. Going to implement as a variable for now...
    def generator_output_bound_rule(m, generator, t):
        if generator not in m.new_dispatchable_generators:
            return (float(Generation_Minimum.loc[t_dict[t],generator]), float(Generation_Maximum.loc[t_dict[t],generator]))
        else:
            a=1
    # Dispatch for each generator
    m.Generator_Power_Output =  Var(m.Generators, m.Time, within=NonNegativeReals)#,bounds=generator_output_bound_rule)
    #set a bound on the the transmission network flow
    def p_rn_line_bound_rule(m, line, t):
        return (-float(transmission_limits.loc[line]['Transmission Limit Reverse [MW]']), float(transmission_limits.loc[line]['Transmission Limit Forward [MW]']))

    # Power flow over AC transmission lines
    m.AC_Transmission_Power_Flow = Var(m.AC_Lines, m.Time,bounds=p_rn_line_bound_rule, within=Reals)

    # Dummy variables used to compute absolute flows over AC link
    m.Power_AC_Interconnector_up = Var(m.AC_Lines, m.Time, within=NonNegativeReals)
    m.Power_AC_Interconnector_lo = Var(m.AC_Lines, m.Time, within=NonNegativeReals)
    
    #Variables to add standard and peak pricing
    m.Peak_Pricing_Dispatch = Var(m.Generators, m.Time, within=NonNegativeReals)
    m.Standard_Pricing_Dispatch = Var(m.Generators, m.Time, within=NonNegativeReals)

    # The wind limit has been implemented here, Solar must be implemented as a constraint because the limit considers solar thermal and PV.

    if simulate_REZ:
        def REZ_Solar_Bound(m, REZ):
            return (float(0), float(Renewable_Build_Limit.loc[REZ, 'Solar PV plus Solar thermal Limits (MW)1']))
        def REZ_Wind_Bound(m, REZ):
            return (float(0), float(Renewable_Build_Limit.loc[REZ, 'Medium Wind generation limits (MW)']))
        def REZ_Solar_Bound_dummy(m, REZ):
            return (float(0), float(0))
        def REZ_Wind_Bound_dummy(m, REZ):
            return (float(0), float(0))

        m.Zone_New_Solar_Capacity = Var(m.Solar_REZ, within=NonNegativeReals, bounds=REZ_Solar_Bound)
        m.Zone_New_Wind_Capacity = Var(m.Wind_REZ, within=NonNegativeReals,bounds=REZ_Wind_Bound)
        m.Zone_New_Renewable_Generation = Var(m.NEM_Zones, m.Time, within=NonNegativeReals)
        m.Zone_New_Wind_Generation= Var(m.NEM_Zones, m.Time, within=NonNegativeReals)
        m.Zone_New_Solar_Generation= Var(m.NEM_Zones, m.Time, within=NonNegativeReals)

    return m

############################################################################################################

def create_model_expressions(m,Coal_Generators):

    # Expressions
    # -----------
    # Total power output for each generator
    def Generator_Power_Output_rule(m, generator, t):
        if generator not in Coal_Generators:
            return (m.Generator_Power_Output_Above_Pmin[generator, t])
        else:
            if generator not in m.new_dispatchable_generators:
                return ((m.P_MIN[generator] + m.Generator_Power_Output_Above_Pmin[generator, t]))
            else:
                return ((m.P_MIN[generator] * m.new_dispatchable_generator_Capacity[generator] + m.Generator_Power_Output_Above_Pmin[generator, t]))

    m.Generator_Power_Output = Expression(m.Generators, m.Time, rule=Generator_Power_Output_rule)

    # ensure that the change in generator power does not exceed Generator Ramp rate
    def Power_Ramp_rule(m, generator, t):
        if t != m.Time.first():
            return m.Generator_Power_Output_Above_Pmin[generator, t] == m.Generator_Power_Output_Above_Pmin[generator, t-1]+m.Generator_Power_Change[generator,t]
        else:
            return Constraint.Skip
        #else:
        #    if generator in Coal_Generators:
        #        if generator not in m.new_dispatchable_generators:
        #            return m.Generator_Power_Output_Above_Pmin[generator, t]==m.P_MIN[generator]+m.Generator_Power_Change[generator,t]
        #        else:
        #            return (m.Generator_Power_Output_Above_Pmin[generator, t] == m.P_MIN[generator] * m.new_dispatchable_generator_Capacity[generator] +
        #                   m.Generator_Power_Change[generator, t])

        #    else:
        #        return m.Generator_Power_Output_Above_Pmin[generator, t]==0+m.Generator_Power_Change[generator,t]

    m.Power_Ramp=Constraint(m.Generators, m.Time, rule=Power_Ramp_rule)

    #constraints
    # Limit the upper bound of Generator_Power_Change to the ramp up rates
    def Power_Ramp_Up_rule(m, generator, t):
        if t != m.Time.first():
            if generator not in m.new_dispatchable_generators:
                return (m.Generator_Power_Change[generator,t]<=m.Ramp_UP[generator])
            else:

                return (m.Generator_Power_Change[generator, t] <= m.Ramp_UP[generator] * m.new_dispatchable_generator_Capacity[generator])
        else:
            return Constraint.Skip

    m.Power_Ramp_Up=Constraint(m.Generators, m.Time, rule=Power_Ramp_Up_rule)

    # Limit the Lower bound of Generator_Power_Change to the neg ramp down rate
    def Power_Ramp_Down_rule(m, generator, t):
        if t != m.Time.first():
            if generator not in m.new_dispatchable_generators:
                return (m.Generator_Power_Change[generator,t] >= -m.Ramp_Down[generator])
            else:
                return (m.Generator_Power_Change[generator, t] >= -m.Ramp_Down[generator] * m.new_dispatchable_generator_Capacity[generator])
        else:
            return Constraint.Skip

    m.Power_Ramp_Down=Constraint(m.Generators, m.Time, rule=Power_Ramp_Down_rule)

    # Energy output for each generator
    def Output_Rule(m, generator, t):
        #if t != m.Time.first():
        #    return ((m.Generator_Power_Output[generator, t - 1] + m.Generator_Power_Output[generator, t]) / 2) * m.DELTA
        #else:
        return (m.Generator_Power_Output[generator, t])

    m.Generator_Dispatch_Above_Pmin = Expression(m.Generators, m.Time, rule=Output_Rule)

    return m

def create_objective():
    a=1
    